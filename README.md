# Project has moved to [https://gitlab.com/open-radio/OpenRadio](https://gitlab.com/open-radio/OpenRadio)
# OpenRadio an Open Source Radio App written in Python.
[![pkgversion](https://img.shields.io/pypi/v/openradio)](https://pypi.org/project/openradio/)
[![repository](https://img.shields.io/badge/src-GitLab-orange)](https://gitlab.com/1337Misom/openradio)
[![versionsupport](https://img.shields.io/pypi/pyversions/openradio)](https://pypi.org/project/openradio/)
[![license](https://img.shields.io/badge/license-GPLv3-orange)](https://gitlab.com/1337Misom/OpenRadio/-/blob/main/LICENSE.md)

## Quick Start
To install from PyPi:
1. You must have at least Python 3.10 and pip 23.0.0
2. Make sure you have [pip](https://pip.pypa.io/en/stable/index.html) installed
3. Install the Dependencies
3. Install with `python3 -m pip install OpenRadio`
4. Run by typing `openradio` in your terminal
5. If you want DAB support install [simple_dab_lib](https://gitlab.com/1337Misom/simple_dab_lib)

## Dependencies
On Ubuntu install with: `sudo apt install libgtk-3-dev portaudio19-dev libpython3-dev pkg-config libboost-all-dev`
 
## Documentation
For documentation please consult the [wiki](https://gitlab.com/1337Misom/OpenRadio/-/wikis/home).

## OpenRadio in action
![Ubuntu Dark](pictures/ubuntu_dark.png)

## What it can do
- Help you with standing up in the morning
- Combine Alarm,DAB and InternetRadio into one
- Simple interface for adding new modules
- Dynamically load Modules
- It also has a Clock

## Where it can run on
- On your Unix machine
- On Raspberry Pis
- On Phones supporting Gtk and Python

## What it can't do
- Bring you Breakfast in the morning


## Social
[Mastodon](https://fosstodon.org/@openradio)
