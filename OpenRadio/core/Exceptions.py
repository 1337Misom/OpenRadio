# Raised when the http api is unable to startup
class HTTPApiNotStarting(Exception):
    pass
