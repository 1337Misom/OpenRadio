��          ,      <       P   �   Q     I    ^                     An open-source, touch-focused radio application which is written in Python.
The Gui was built using Gtk3.
And it is Modular AF.
For more info visit <a href="https://gitlab.com/1337Misom/OpenRadio">Gitlab</a>.
OpenRadio Copyright (C) 2023 1337Misom Project-Id-Version: PACKAGE VERSION
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Generated-By: pygettext.py 1.5
 Ein Open-Source und Touch fokusiertes program, welches in Python geschrieben wurde.
Die GUI wurde mit Gtk3 gebaut.
Es ist auch modular.
Für mehr infos besuche <a href="https://gitlab.com/1337Misom/OpenRadio">Gitlab</a>.
OpenRadio Copyright (C) 2023 1337Misom 