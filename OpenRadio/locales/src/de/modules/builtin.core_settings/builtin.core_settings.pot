# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR ORGANIZATION
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2023-06-28 21:38+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Generated-By: pygettext.py 1.5\n"


#: ../OpenRadio/OpenRadio//modules/CoreSettings/__init__.py:166
msgid "Fullscreen"
msgstr "Vollbild"

#: ../OpenRadio/OpenRadio//modules/CoreSettings/__init__.py:183
msgid "HTTP Port"
msgstr "HTTP Port"

#: ../OpenRadio/OpenRadio//modules/CoreSettings/__init__.py:198
msgid "Only use backup icons"
msgstr "Nur backup icons verwenden"

#: ../OpenRadio/OpenRadio//modules/CoreSettings/__init__.py:238
msgid "General"
msgstr "Allgemein"

#: ../OpenRadio/OpenRadio//modules/CoreSettings/__init__.py:239
msgid "Module order"
msgstr "Modulreihenfolge"
