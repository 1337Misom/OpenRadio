# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR ORGANIZATION
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2023-06-25 16:14+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Generated-By: pygettext.py 1.5\n"


#: ../OpenRadio_test/OpenRadio//modules/About/__init__.py:31
msgid ""
"An open-source, touch-focused radio application which is written in Python.\n"
"The Gui was built using Gtk3.\n"
"And it is Modular AF.\n"
"For more info visit <a href=\"https://gitlab.com/1337Misom/OpenRadio\">Gitlab</a>.\n"
"OpenRadio Copyright (C) 2023 1337Misom"
msgstr ""
"Ein Open-Source und Touch fokusiertes program, welches in Python geschrieben wurde.\n"
"Die GUI wurde mit Gtk3 gebaut.\n"
"Es ist auch modular.\n"
"Für mehr infos besuche <a href=\"https://gitlab.com/1337Misom/OpenRadio\">Gitlab</a>.\n"
"OpenRadio Copyright (C) 2023 1337Misom"
