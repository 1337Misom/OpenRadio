# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR ORGANIZATION
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2023-06-25 16:16+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Generated-By: pygettext.py 1.5\n"


#: ../OpenRadio_test/OpenRadio//modules/InternetRadio/UI.py:448
msgid "Search by region"
msgstr ""

#: ../OpenRadio_test/OpenRadio//modules/InternetRadio/UI.py:455
msgid "Search by tag"
msgstr ""

#: ../OpenRadio_test/OpenRadio//modules/InternetRadio/UI.py:462
msgid "Search by language"
msgstr ""

#: ../OpenRadio_test/OpenRadio//modules/InternetRadio/UI.py:469
msgid "Favorites"
msgstr ""

