# Code
- All the code you want to contribute has to be licensed under GPL-3.0
- Your code must be formatted using `black`
- You should follow general python [rules](https://peps.python.org/pep-0008) e.g. CamelCase for classes and lowercase with underscores for variables
- For docs look at the [Gitlab wiki](https://gitlab.com/1337Misom/OpenRadio/-/wikis/home)
# Translations
- You should be decent in the language you want to submit
- Try to use english as the base
- The translations are also gonna be licensed under GPL-3.0
- For docs look at the [Gitlab wiki](https://gitlab.com/1337Misom/OpenRadio/-/wikis/home)
